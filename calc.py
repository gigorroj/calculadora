def suma(n1, n2):
    return n1 + n2


def resta(n3, n4):
    return n3 - n4


if __name__ == "__main__":

    print("La suma de 1 y 2 es: " + str(suma(1, 2)))
    print("La suma de 3 y 4 es: " + str(suma(3, 4)))
    print("La resta de 5 a 6 es: " + str(resta(6, 5)))
    print("La resta de 7 a 8 es: " + str(resta(8, 7)))
